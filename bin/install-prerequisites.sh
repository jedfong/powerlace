#!/bin/bash
if [[ ! -e ~/.zshrc ]]; then
  touch ~/.zshrc && chmod 600 ~/.zshrc
fi

which node > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo Installing nvm...
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash

  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
  echo Installing node...
  nvm install --lts node
fi

which brew > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo Installing brew...
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

which mas > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo Installing mas...
  brew install mas
fi

echo Complete the install by opening a new terminal session
